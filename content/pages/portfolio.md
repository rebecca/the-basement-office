+++
title = "Portfolio"
slug = "portfolio"
template = "portfolio.html"
path = "portfolio"
[extra]
navcolor = "light"
+++

I’ve collected some samples to specifically demonstrate different types of content or different creation/collaboration processes. There's a range of topics and technical depth: from defining the basics, to socio-technical and cultural issues, to diving deep into a technical challenge or project. Feel free to reach out if you’d like to see more. 
